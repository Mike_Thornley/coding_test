<?php

/**
 * A database class to handle the connection the database and run the queries.
 * @author Michael Thornley
 *
 */
class SQL
{
	/**
	 * Instantiate our SQL instance, DB connection
	 * @param unknown $hostname
	 * @param unknown $username
	 * @param unknown $password
	 * @param unknown $database
	 * @throws exception
	 */
	public function __construct($hostname,$username,$password,$database)
	{
		$this->conn = new mysqli($hostname,$username,$password,$database);
		if($this->conn->connect_errno)
		{
			throw new exception('Database failed to connect: ' . $this->conn->connect_errno);
		}
		
		return $this->conn;
	}
	
	/**
	 * Execute a SQL query, with data returned
	 * @param unknown $query is the query string we pass in.
	 */
	public function executeSQL($query)
	{
		try
		{
			$this->resultSet = $this->conn->query($query);
			
			/**
			 * If there are no results returned, return null
			 */
			if(!$this->resultSet->num_rows)
			{
				return false;				
			}	
		
			$i=0;
			while($row = $this->resultSet->fetch_object())
			{
				if(!isset($this->resultsObj))
				{
					$this->resultsObj = new stdClass;
					
				}
				$this->resultsObj->{$i++} = $row;
			}
				
			return $this->resultsObj;
		}
		
		catch(Exception $e)
		{
			echo $ex->getMessage();
		}
	}
	
	/**
	 * Execute SQL query with no results set returned - used for update, delete etc.
	 * @param unknown $query is the query string we pass in. 
	 */
	public function executeSQLNoData($query)
	{
		try
		{
			$this->resultSet = $this->conn->query($query);
			return $this->resultSet;
		}
		
		catch(Exception $e)
		{
			echo $ex->getMessage();
		}
	}
	
	/**
	 * Simple function to sanitise DB input so we don't get injected. 
	 * @param unknown $string are the DB variables one by one. 
	 */
	public function sanitiseVars($string)
	{
		$string = htmlentities($string);
		$string = strip_tags($string);
		$string = stripcslashes($string);
		return $string = addslashes($string);
	}
	
	/**
	 * Close the DB connection
	 */
	public function __destruct()
	{
		$this->conn->close();
	}
}