CREATE DATABASE scrabble;
USE scrabble;

CREATE TABLE member
(
member_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
first_name VARCHAR(25),
last_name VARCHAR(25),
date_joined DATETIME, 
contact_number VARCHAR(12),
enabled INT DEFAULT 1
);

INSERT INTO member(first_name,last_name,date_joined,contact_number,enabled)
VALUES('Mike', 'Thornley',DATE_SUB(NOW(),INTERVAL 3 YEAR), '07912762045',1),
('John', 'Smith', DATE_SUB(NOW(),INTERVAL 1 YEAR), '08962754034',1),
('James', 'Jones', DATE_SUB(NOW(),INTERVAL 2 YEAR), '07963451089',1),
('Alex', 'Jackson', DATE_SUB(NOW(),INTERVAL 1 YEAR), '07835190764',1),
('John', 'James', DATE_SUB(NOW(),INTERVAL 3 YEAR), '09834190453',1);

CREATE TABLE games
(
game_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
duration_minutes SMALLINT, 
when_played DATETIME,
where_played VARCHAR(25)
);

INSERT INTO games(duration_minutes,when_played,where_played)
VALUES('60',NOW(),'London'),
('60',DATE_SUB(NOW(),INTERVAL 1 WEEK),'London'),
('160',DATE_SUB(NOW(),INTERVAL 2 WEEK),'Londonderry'),
('90',DATE_SUB(NOW(),INTERVAL 1 MONTH),'Luton'),
('30',DATE_SUB(NOW(),INTERVAL 6 MONTH),'Leceister'),
('30',DATE_SUB(NOW(),INTERVAL 8 MONTH),'Barnsley'),
('20',DATE_SUB(NOW(),INTERVAL 3 MONTH),'Stoke'),
('80',DATE_SUB(NOW(),INTERVAL 2 WEEK),'Manchester'),
('10',DATE_SUB(NOW(),INTERVAL 6 WEEK),'London'),
('50',DATE_SUB(NOW(),INTERVAL 9 WEEK),'London'),
('120',DATE_SUB(NOW(),INTERVAL 5 DAY),'Preston');

CREATE TABLE scores
(
score_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
game_id INT,
member_id INT,
score INT,
winner INT
);

INSERT INTO scores(game_id,member_id,score,winner)
VALUES('1','1','800',1),
('1','2','200',0),
('2','4','800',1),
('2','2','200',0),
('3','1','100',0),
('3','5','700',1),
('4','1','170',0),
('4','4','1200',1),
('5','5','300',0),
('5','3','700',1),
('6','2','1600',1),
('6','4','700',0),
('7','1','1100',1),
('7','2','500',0),
('8','3','600',1),
('8','2','200',0),
('9','5','100',1),
('9','2','70',0),
('10','4','100',0),
('10','2','700',1),
('11','1','700',0),
('11','2','3000',1);


